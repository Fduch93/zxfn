#include "Z_Step.h"

#include "Z_Xfn.h"
#include "Z_Cycle.h"
#include "Z_Pascal.h"
#include "Z_Print.h"

#ifdef Z_LOG_ANALYZER

	extern volatile float __Z_LOG_SRC;
	extern volatile float __Z_LOG_EN;

	#define Z_LOG_EN(en)  __Z_LOG_EN = en
	#define Z_LOG_SRC(val)  if (__Z_LOG_EN) __Z_LOG_SRC = val

#else

	#define Z_LOG_EN(en)
	#define Z_LOG_SRC(val)

#endif

#ifdef TEST

typedef struct {
	float *xfn;
	int    sz ;
} Z_StepTestXfn_S;

static Z_StepTestXfn_S Z_StepTestXfn = {0};

static float Z_XfnTestCycleDirect(float x) { return Z_CycleDirect(Z_StepTestXfn.xfn, Z_StepTestXfn.sz, x); }
static float Z_XfnTestCycleCanon (float x) { return Z_CycleCanon (Z_StepTestXfn.xfn, Z_StepTestXfn.sz, x); }

typedef struct
{
	float *xfn   ;
	int    sz    ;
	float  fFlt  ;
	float  fSmpl ;
	float  t     ;
} Z_StepConfig_S;

static void Z_Step(Z_StepConfig_S *cfg);
static void Z_DoStep(float (*xf)(float), float fs, float T);

#define Z_STEP_MAX_SZ 8

// wolfram (0.204+0.167s^2+0.0297s^4)/(0.204+0.747s^1+1.097s^2+1.815s^3+0.925s^4+s^5) bode
static float Z_Xfn_1[6 * 6] = {
	0.204,     0, 0.167,     0, 0.0297, 0,
	0.204, 0.747, 1.097, 1.815, 0.925 , 1
};

// wolfram (4s+1)(0.1s+1)/(2s^2+2s+1) bode 
static float Z_Xfn_2[3 * 6] = {
	1, 4.1, 0.4,
	1, 2  , 2
};

//                              xfn    sz  fFlt  fSmpl  t
static Z_StepConfig_S s1 = { Z_Xfn_1,  6,   50,  2000,  1 };
static Z_StepConfig_S s2 = { Z_Xfn_2,  3,    1,   500,  5 };

void Z_StepTest(void)
{
//	Z_PrintReset();
	Z_Step(&s1);
	Z_Step(&s2);
}

static int32_t Z_StepPascal[Z_STEP_MAX_SZ * Z_STEP_MAX_SZ];
static float   Z_StepXfn   [Z_STEP_MAX_SZ * 6            ];

static void Z_Step(Z_StepConfig_S *cfg)
{
	for (int i = 0; i < cfg->sz*2; i++)
		Z_StepXfn[i] = cfg->xfn[i];
	
	for (int i = cfg->sz*2; i <= cfg->sz*6; i++)
		Z_StepXfn[i] = 0;
	
	Z_InitPascal (Z_StepPascal, cfg->sz);
	Z_PrintPascal(Z_StepPascal, cfg->sz);
	
	float c = Z_CalcFreqKoef(cfg->fFlt, cfg->fSmpl);
	
	Z_XfnConvertS2Z(Z_StepXfn, cfg->sz, Z_StepPascal, c);
	Z_Print("Source s-xfn"   , Z_StepXfn + 0*cfg->sz, cfg->sz, "s");
	Z_Print("Converted z-xfn", Z_StepXfn + 1*cfg->sz, cfg->sz, "z");
	
	Z_StepTestXfn.xfn = Z_StepXfn;
	Z_StepTestXfn.sz  = cfg->sz;
	
	Z_DoStep(Z_XfnTestCycleDirect, cfg->fSmpl, cfg->t); 
	Z_DoStep(Z_XfnTestCycleCanon , cfg->fSmpl, cfg->t);
}

#include <math.h>

#define PI 3.141592653589793f

static void Z_DoStep(float (*xf)(float), float fs, float T)
{
	Z_LOG_EN(0);
	
	float t = 0.0f;
	
	while (t < T)
	{
		float x = 0;
		
		Z_LOG_SRC(x);
		
		xf(x);
		
		t += 1/fs;
	}
	
	Z_LOG_EN(1);
	
	while (t < 2*T)
	{
		float x = 1;
		
		Z_LOG_SRC(x);
		
		xf(x);
		
		t += 1/fs;
	}
	
	while (t < 3*T)
	{
		float x = 0;
		
		Z_LOG_SRC(x);
		
		xf(x);
		
		t += 1/fs;
	}
}

#endif
