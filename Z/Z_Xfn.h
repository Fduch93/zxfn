#ifndef __Z_XFN
#define __Z_XFN

#include <stdint.h>

#define DEFINE_XFN_SZ(name,sz) \
                               \
typedef union {                \
  float xfn[sz*4];             \
  struct {                     \
    float S[sz*2];             \
    float Z[sz*2];             \
  };                           \
  struct {                     \
    float a[sz  ];             \
    float b[sz  ];             \
    float A[sz  ];             \
    float B[sz  ];             \
  };                           \
} name ## _U;                  \
                               \
typedef struct {               \
  name ## _U xfn;              \
  union {                      \
    float   buf [sz];          \
    struct {                   \
      float xPre[sz];          \
      float yPre[sz];          \
    };                         \
  };                           \
  uint32_t pos;                \
} name ## _S;


DEFINE_XFN_SZ(Z_Xfn1, 2)
DEFINE_XFN_SZ(Z_Xfn2, 3)
DEFINE_XFN_SZ(Z_Xfn3, 4)
DEFINE_XFN_SZ(Z_Xfn4, 5)
DEFINE_XFN_SZ(Z_Xfn5, 6)
DEFINE_XFN_SZ(Z_Xfn6, 7)
DEFINE_XFN_SZ(Z_Xfn7, 8)

void Z_XfnNormalize (float *ab , uint32_t sz);
void Z_XfnConvertS2Z(float *xfn, uint32_t sz, int32_t *P, float c);
void Z_XfnConvertZ2S(float *xfn, uint32_t sz, int32_t *P, float c);

float Z_CalcFreqKoef(float freqFilter, float freqCycle);

#ifdef TEST

void Z_XfnConvertTest(void);

#endif

#endif
