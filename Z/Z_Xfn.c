#include "Z_Xfn.h"

#include "Z_Pascal.h"

void Z_XfnNormalize(float *ab, uint32_t sz)
{
	float b = 0;
	{
		float *pb = ab + sz;
		
		for (int i = sz; i > 0; i--)
		{
			if ((b = *(pb++)) != 0)
				break;
		}
	}
	
	if (b == 0)
	{
		float *pb = ab;
		
		for (int i = sz; i > 0; i--)
		{
			b += *(pb++);
		}
	}
	
	for (int k = 2*sz; k > 0; k--)
	{
		*(ab++) /= b;
	}
}

void Z_XfnConvertS2Z(float *xfn, uint32_t sz, int32_t *P, float c)
{
	float *ab =  xfn      ;
	float *AB = &xfn[sz*2];
	
	for (int k = 0; k < sz; k++)
	{
		AB[k   ] = 0;
		AB[k+sz] = 0;
		
		for (int i = 0; i < sz; i++)
		{
			float x = 0;
			
			for (int p =      i; p >= 0; p--)
			for (int q = sz-1-i; q >= 0; q--)
				if (p+q == k)
				{
					float xx = Z_PASCAL_VALUE(P, sz,      i, p)
					         * Z_PASCAL_VALUE(P, sz, sz-1-i, q);
					
					if (p & 1)
						xx = -xx;
					
					x += xx;
				}
			
			for (int e = 0; e < i; e++)
				x *= c;
			
			AB[k   ] += ab[i   ] * x;
			AB[k+sz] += ab[i+sz] * x;
		}
	}
	
	Z_XfnNormalize(AB, sz);
}

void Z_XfnConvertZ2S(float *xfn, uint32_t sz, int32_t *P, float c)
{
	float *ab =  xfn      ;
	float *AB = &xfn[sz*2];
	
	for (int k = 0; k < sz; k++)
	{
		ab[k   ] = 0;
		ab[k+sz] = 0;
		
		float cc = 1;
		for (int e = sz-1-k; e > 0; e--)
			cc *= c;
		
		for (int i = 0; i < sz; i++)
		{
			float x = 0;
			
			for (int p =      i; p >= 0; p--)
			for (int q = sz-1-i; q >= 0; q--)
			{
				if (p+q == k)
				{
					float xx = Z_PASCAL_VALUE(P, sz,      i, p)  //P[(     i)*sz+p]
					         * Z_PASCAL_VALUE(P, sz, sz-1-i, q); //P[(sz-1-i)*sz+q];
					
					if (p & 1)
						xx = -xx;
					
					x += xx * cc;
				}
			}
			
			ab[k   ] += AB[i   ] * x;
			ab[k+sz] += AB[i+sz] * x;
		}
	}
	
	Z_XfnNormalize(ab, sz);
}

float Z_CalcFreqKoef(float freqFilter, float freqCycle)
{
	float q = 3.14159265358979283f * freqFilter / freqCycle;
	
	float     qq  = 1/q
	              -   q*(1.f/3  );
	q *= q*q; qq -=   q*(1.f/45 );
	q *= q*q; qq -=   q*(2.f/945);
	
	return qq; // ~= ctg(pi*freqFilter/freqCycle)
}

#ifdef TEST

#include "Z_Print.h"

#pragma diag_suppress 47

void Z_XfnConvertTest(void)
{
	// wolfram 1/(1+0.101s^1+0.0001s^2) bode 
	{
		#define SZ 3
		DEFINE_XFN_SZ(Z_Xfn2, SZ)
		
		int32_t   pascal[SZ*SZ] = {0};
		Z_Xfn2_U  xfnZ2S        = {0};
		Z_Xfn2_U  xfnS2Z        = { .S = {
			1, 0, 0,
			1, 0.101, 0.0001
		}};
		
		Z_InitPascal(pascal, SZ);
		Z_PrintPascal(pascal, SZ);
		
		Z_XfnConvertS2Z(xfnS2Z.xfn, SZ, pascal, 1);
		Z_Print("Source s-xfn"   , xfnS2Z.S, SZ, "s");
		Z_Print("Converted z-xfn", xfnS2Z.Z, SZ, "z");
		
		for (int i = 0; i < SZ*2; i++)
			xfnZ2S.Z[i] = xfnS2Z.Z[i];
		
		Z_XfnConvertZ2S(xfnZ2S.xfn, SZ, pascal, 1);
		Z_Print("Source z-xfn"   , xfnZ2S.Z, SZ, "z");
		Z_Print("Converted s-xfn", xfnZ2S.S, SZ, "s");
		
		Z_XfnNormalize(xfnS2Z.S, SZ);
		Z_Print("Normalized source s-xfn", xfnS2Z.S, SZ, "s");
	}
	// wolfram (0.204+0.167s^2+0.0297s^4)/(0.204+0.747s^1+1.097s^2+1.815s^3+0.925s^4+s^5) bode 
	{
		#define SZ 6
		DEFINE_XFN_SZ(Z_Xfn5, SZ)
		
		int32_t   pascal[SZ*SZ] = {0};
		Z_Xfn5_U  xfnZ2S        = {0};
		Z_Xfn5_U  xfnS2Z        = { .S = {
			0.204,     0, 0.167,     0, 0.0297, 0,
			0.204, 0.747, 1.097, 1.815, 0.925 , 1
		}};
		
		Z_InitPascal(pascal, SZ);
		Z_PrintPascal(pascal, SZ);
		
		float c = Z_CalcFreqKoef(200, 2000); // expected 3.07769012 (3.0776835371752534025702905760369)
		
		Z_XfnConvertS2Z(xfnS2Z.xfn, SZ, pascal, c);
		Z_Print("Source s-xfn"   , xfnS2Z.S, SZ, "s");
		Z_Print("Converted z-xfn", xfnS2Z.Z, SZ, "z");
		
		for (int i = 0; i < SZ*2; i++)
			xfnZ2S.Z[i] = xfnS2Z.Z[i];
		
		Z_XfnConvertZ2S(xfnZ2S.xfn, SZ, pascal, c);
		Z_Print("Source z-xfn"   , xfnZ2S.Z, SZ, "z");
		Z_Print("Converted s-xfn", xfnZ2S.S, SZ, "s");
		
		Z_XfnNormalize(xfnS2Z.S, SZ);
		Z_Print("Normalized source s-xfn", xfnS2Z.S, SZ, "s");
	}
}

#endif
