#include "Z_Pascal.h"

#if defined(Z_PASCAL_MATRIX)

void Z_InitPascal(int32_t *p, uint32_t sz)
{
	for (int i = 0; i < sz; i++)
	{
		p[0] = 1;
		p[i] = 1;
		
		int32_t *pp = p - sz;
		
		for (int j = 1; j < i; j++)
		{
			int ppp = *(pp++);
			p[j] = *pp + ppp;
		}
		
		p += sz;
	}
}

#if defined(Z_PASCAL_VALUE_FUNC)
int32_t Z_PascalValue(int32_t *p, int32_t sz, uint32_t r, uint32_t c)
{
	return p[r*sz+c];
}
#endif

#elif defined(Z_PASCAL_ARRAY)

void Z_InitPascal(int32_t *p, uint32_t sz)
{
	*(p++) = 1;
	
	for (int i = 1; i < sz; i++)
	{
		*(p++) = 1;
		
		for (int j = 1; j < i; j++)
		{
			int32_t *pp = p-i-1;
			*(p++) = pp[0] + pp[1];
		}
		
		*(p++) = 1;
	}
}

#if defined(Z_PASCAL_VALUE_FUNC)
int32_t Z_PascalValue(int32_t *p, uint32_t r, uint32_t c)
{
	return p[r*(r+1)/2U + c];
}
#endif

#endif
