#include "Z_Sweep.h"

#include "Z_Xfn.h"
#include "Z_Cycle.h"
#include "Z_Pascal.h"
#include "Z_Print.h"

#ifdef Z_LOG_ANALYZER

	extern volatile float __Z_LOG_SRC;
	extern volatile float __Z_LOG_EN;

	#define Z_LOG_EN(en)  __Z_LOG_EN = en
	#define Z_LOG_SRC(val)  if (__Z_LOG_EN) __Z_LOG_SRC = val

#else

	#define Z_LOG_EN(en)
	#define Z_LOG_SRC(val)

#endif

#ifdef TEST

typedef struct {
	float *xfn;
	int    sz ;
} Z_TestXfn_S;

static Z_TestXfn_S Z_TestXfn = {0};

static float Z_XfnTestCycleDirect(float x) { return Z_CycleDirect(Z_TestXfn.xfn, Z_TestXfn.sz, x); }
static float Z_XfnTestCycleCanon (float x) { return Z_CycleCanon (Z_TestXfn.xfn, Z_TestXfn.sz, x); }

typedef struct
{
	float *xfn   ;
	int    sz    ;
	float  fFlt  ;
	float  fSmpl ;
	float  fStart;
	float  fEnd  ;
	float  kf    ;
	int    n     ;
} Z_SweepConfig_S;

static void  Z_Sweep(Z_SweepConfig_S *cfg);
static float Z_AnalyzeMax(float (*xf)(float), float fs, float fSig, int n);

#define Z_SWEEP_MAX_SZ 8

#define KF_10  1.2589254117941672104239541063958f // 10^(1/10)  ~  10 pts per decade
#define KF_20  1.1220184543019634355910389464779f // 10^(1/20)  ~  20 pts per decade
#define KF_50  1.0471285480508995334645020315281f // 10^(1/50)  ~  50 pts per decade
#define KF_100 1.0232929922807541309662751748199f // 10^(1/100) ~ 100 pts per decade

// wolfram (0.204+0.167s^2+0.0297s^4)/(0.204+0.747s^1+1.097s^2+1.815s^3+0.925s^4+s^5) bode
static float Z_Xfn_1[6 * 6] = {
	0.204,     0, 0.167,     0, 0.0297, 0,
	0.204, 0.747, 1.097, 1.815, 0.925 , 1
};

// wolfram (4s+1)(0.1s+1)/(2s^2+2s+1) bode 
static float Z_Xfn_2[3 * 6] = {
	1, 4.1, 0.4,
	1, 2  , 2
};

//                              xfn    sz  fFlt  fSmpl  fStart   fEnd  kf     n 
static Z_SweepConfig_S s1 = { Z_Xfn_1,  6,   50,  2000,     10,   100, KF_20, 8 };
static Z_SweepConfig_S s2 = { Z_Xfn_2,  3,    1,   500,    0.1,   100, KF_10, 3 };

void Z_SweepTest(void)
{
//	Z_PrintReset();
	Z_Sweep(&s1);
	Z_Sweep(&s2);
}

static int32_t Z_SweepPascal[Z_SWEEP_MAX_SZ * Z_SWEEP_MAX_SZ];
static float   Z_SweepXfn   [Z_SWEEP_MAX_SZ * 6             ];

static void Z_Sweep(Z_SweepConfig_S *cfg)
{
	for (int i = 0; i < cfg->sz*2; i++)
		Z_SweepXfn[i] = cfg->xfn[i];
	
	for (int i = cfg->sz*2; i <= cfg->sz*6; i++)
		Z_SweepXfn[i] = 0;
	
	Z_InitPascal (Z_SweepPascal, cfg->sz);
	Z_PrintPascal(Z_SweepPascal, cfg->sz);
	
	float c = Z_CalcFreqKoef(cfg->fFlt, cfg->fSmpl);
	
	Z_XfnConvertS2Z(Z_SweepXfn, cfg->sz, Z_SweepPascal, c);
	Z_Print("Source s-xfn"   , Z_SweepXfn + 0*cfg->sz, cfg->sz, "s");
	Z_Print("Converted z-xfn", Z_SweepXfn + 1*cfg->sz, cfg->sz, "z");
	
	Z_TestXfn.xfn = Z_SweepXfn;
	Z_TestXfn.sz  = cfg->sz;
	
	for (float f = cfg->fStart; f < cfg->fEnd*cfg->kf; f *= cfg->kf)
	{
		float max = Z_AnalyzeMax(Z_XfnTestCycleDirect, cfg->fSmpl, f, cfg->n); 
		
//		Z_Printf("%.3f,%.3f\n", f, max);
	}
	
	for (float f = cfg->fStart; f < cfg->fEnd*cfg->kf; f *= cfg->kf)
	{
		float max = Z_AnalyzeMax(Z_XfnTestCycleCanon, cfg->fSmpl, f, cfg->n); 
		
//		Z_Printf("%.3f,%.3f\n", f, max);
	}
}

#include <math.h>

#define PI 3.141592653589793f

static float Z_AnalyzeMax(float (*xf)(float), float fs, float fSig, int n)
{
	Z_LOG_EN(0);
	
	float t = 0.0f;
	
	while (t < 2/fSig)
	{
		float x = sinf(2*PI*fSig*t);
		
		Z_LOG_SRC(x);
		
		xf(x);
		
		t += 1/fs;
	}
	
	Z_LOG_EN(1);
	
	while (t < n/fSig)
	{
		float x = sinf(2*PI*fSig*t);
		
		Z_LOG_SRC(x);
		
		xf(x);
		
		t += 1/fs;
	}
	
	float max = 0.0f;
	
	while (t < (n+2)/fSig)
	{
		float x = sinf(2*PI*fSig*t);
		
		Z_LOG_SRC(x);
		
		float y = xf(x);
		
		if (y > max)
			max = y;
		
		t += 1/fs;
	}
	
	return max;
}

#endif
