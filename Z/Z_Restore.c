#include "Z_Restore.h"

#include "MATRIX.h"
#include "SLAU.h"

#define Z_RESTORE_MAX_SZ 8
#define Z_RESTORE_MATRIX_SZ (2 * Z_RESTORE_MAX_SZ - 1)

typedef struct
{
	SLAU_Equasion_S Eq;
	float DataMatrix[MATRIX_SIZE_WITH_DET_CALC_SPACE(Z_RESTORE_MATRIX_SZ)]; // [ (2*sz-1) * (2*sz-1) + DET_CALC_SPACE ]
	float DataVector[                                Z_RESTORE_MATRIX_SZ ]; // [  2*sz-1                              ]
	float Inv       [            Z_RESTORE_MATRIX_SZ*Z_RESTORE_MATRIX_SZ ]; // [ (2*sz-1) * (2*sz-1)                  ]
	float Result    [                                Z_RESTORE_MATRIX_SZ ]; // [  2*sz-1                              ]
	float B0        [                                Z_RESTORE_MATRIX_SZ ]; // [  2*sz-1                              ]
} Z_Restore_S;

Z_Restore_S Z_Restore = {
	.Eq.A      = Z_Restore.DataMatrix,
	.Eq.B      = Z_Restore.DataVector,
	.Eq.Inv    = Z_Restore.Inv       ,
	.Eq.Result = Z_Restore.Result    ,
};

int Z_XfnRestoreZ(float *xfn, uint32_t sz, float *xyArr, uint32_t xyCnt, uint32_t pos)
{
	#define XY_CNT (3*sz-2)
	#define XY_SZ  (2*XY_CNT)
	#define M_SZ   (2*sz-1)
	
	if (xyCnt < XY_CNT)
		return 0;
	
	Z_Restore_S *restore = &Z_Restore;
	
	// Build DataMatrix && DataVector
	{
		float *xy    = xyArr + 2 * pos;
		float *xyEnd = xyArr + 2 * xyCnt;
		
		for (int i = 0; i < XY_CNT; i++)
		{
//			float x = xy[0];
//			float y = xy[1];
			
			restore->DataVector[i] = xy[1];
			
			for (int cx = 0; cx < sz; cx++)
			{
				int rx = i - cx;
				
				if (0 <= rx && rx < M_SZ)
				{
					restore->DataMatrix[rx*M_SZ+cx] = xy[0];
				}
			}
			
			for (int cy = sz; cy < M_SZ; cy++)
			{
				int ry = sz + i - cy - 1;
				
				if (0 <= ry && ry < M_SZ)
				{
					restore->DataMatrix[ry*M_SZ+cy] = xy[1];
				}
			}
				
			xy += 2;
			if (xy >= xyEnd)
				xy -= XY_SZ;
		}
	}
	
	// Solve SLAU
	if (!SLAU_Solve(&restore->Eq))
		return 0;
	
	// Write result to xfn.Z
	{
		float *kx = xfn + sz*2;
		float *ky = xfn + sz*3;
		
		*(kx++) = restore->Result[0];
		*(ky++) = 1;
		
		for (int i = 1; i < sz - 1; i++)
		{
			*(kx++) = restore->Result[i     ];
			*(ky++) = restore->Result[i + sz];
		}
	}
	
	return 1;
}

#ifdef TEST

#include "Z_Xfn.h"
#include "Z_Cycle.h"
#include "Z_Pascal.h"
#include "Z_Print.h"

#ifdef Z_LOG_ANALYZER

	extern volatile float __Z_LOG_SRC;
	extern volatile float __Z_LOG_EN;

	#define Z_LOG_EN(en)  __Z_LOG_EN = en
	#define Z_LOG_SRC(val)  if (__Z_LOG_EN) __Z_LOG_SRC = val

#else

	#define Z_LOG_EN(en)
	#define Z_LOG_SRC(val)

#endif

static int32_t Z_RestorePascal[Z_RESTORE_MAX_SZ * Z_RESTORE_MAX_SZ];
static float   Z_RestoreXfn   [Z_RESTORE_MAX_SZ * 6               ] = {
	1, 4.1, 0.4,
	1, 2  , 2
};

float Z_RestoreXY[32768] = {0};
int   Z_RestoreXYPos = 0;

void Z_TestXfnRestoreZ(void)
{
	int sz = 3;
	float fs = 1000;
	
	Z_InitPascal(Z_RestorePascal, sz);
	
	float c = Z_CalcFreqKoef(1, fs);
	
	Z_XfnConvertS2Z(Z_RestoreXfn, 3, Z_RestorePascal, c);
	Z_Print("Source s-xfn"   , Z_RestoreXfn + 0*sz, sz, "s");
	Z_Print("Converted z-xfn", Z_RestoreXfn + 1*sz, sz, "z");
	
	float *xy = Z_RestoreXY;
	
	Z_LOG_EN(1);
	
	float t = 0.0f;
	
	while (t < 0.1)
	{
		float x = 0; Z_LOG_SRC(x);
		float y = Z_CycleCanon(Z_RestoreXfn, sz, x);
		
		*(xy++) = x;
		*(xy++) = y;
		
		if ((Z_RestoreXYPos += 2) >= sizeof(Z_RestoreXY)/sizeof(float))
			break;
		
		t += 1/fs;
	}
	
	while (Z_RestoreXYPos < sizeof(Z_RestoreXY)/sizeof(float))
	{
		for (float x = 0; x < 9.95; x += 0.1)
		{
			Z_LOG_SRC(x);
			float y = Z_CycleCanon(Z_RestoreXfn, sz, x);
			
			*(xy++) = x;
			*(xy++) = y;
			
			if ((Z_RestoreXYPos += 2) >= sizeof(Z_RestoreXY)/sizeof(float))
				break;
			
			t += 1/fs;
		}
		
		while (t < 0.1+2)
		{
			float x = 1; Z_LOG_SRC(x);
			float y = Z_CycleCanon(Z_RestoreXfn, sz, x);
			
			*(xy++) = x;
			*(xy++) = y;
			
			if ((Z_RestoreXYPos += 2) >= sizeof(Z_RestoreXY)/sizeof(float))
				break;
			
			t += 1/fs;
		}
		
		for (float x = 1; x > 0.05; x -= 0.1)
		{
			Z_LOG_SRC(x);
			float y = Z_CycleCanon(Z_RestoreXfn, sz, x);
			
			*(xy++) = x;
			*(xy++) = y;
			
			if ((Z_RestoreXYPos += 2) >= sizeof(Z_RestoreXY)/sizeof(float))
				break;
			
			t += 1/fs;
		}
		
		while (t < 0.1+2+2)
		{
			float x = 0; Z_LOG_SRC(x);
			float y = Z_CycleCanon(Z_RestoreXfn, sz, x);
			
			*(xy++) = x;
			*(xy++) = y;
			
			if ((Z_RestoreXYPos += 2) >= sizeof(Z_RestoreXY)/sizeof(float))
				break;
			
			t += 1/fs;
		}
		
		t-=2+2;
	}
	Z_LOG_EN(0);
	
	for (int i = 0; i < sizeof(Z_RestoreXY)/sizeof(float)/2 - sz*2; i++)
	{
		int ok = Z_XfnRestoreZ(Z_RestoreXfn, sz, Z_RestoreXY, sizeof(Z_RestoreXY)/sizeof(float)/2, i);
		
		if (ok)
		{
			Z_XfnConvertZ2S(Z_RestoreXfn, sz, Z_RestorePascal, c);
			Z_Print("Source s-xfn"   , Z_RestoreXfn + 0*sz, sz, "s");
			Z_Print("Converted z-xfn", Z_RestoreXfn + 1*sz, sz, "z");
		}
		else
		{
			Z_Printf("FAIL\n");
		}
	}
}

#endif
