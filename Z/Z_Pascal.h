#ifndef __Z_PASCAL
#define __Z_PASCAL

#include <stdint.h>

//#define Z_PASCAL_MATRIX
#define Z_PASCAL_ARRAY

//#define Z_PASCAL_VALUE_FUNC
#define Z_PASCAL_VALUE_MACRO

void Z_InitPascal(int32_t *p, uint32_t sz);

#if defined(Z_PASCAL_MATRIX)

#if defined(Z_PASCAL_VALUE_FUNC)
	#define Z_PASCAL_VALUE(p,sz,r,c) Z_PascalValue((p),(sz),(r),(c))
	int32_t Z_PascalValue(int32_t *p, int32_t sz, uint32_t r, uint32_t c);
#elif defined(Z_PASCAL_VALUE_MACRO)
	#define Z_PASCAL_VALUE(p,sz,r,c) ((p)[(r)*(sz)+(c)])
#endif

#elif defined(Z_PASCAL_ARRAY)

#if defined(Z_PASCAL_VALUE_FUNC)
	#define Z_PASCAL_VALUE(p,sz,r,c) Z_PascalValue((p),(r),(c))
	int32_t Z_PascalValue(int32_t *p, uint32_t r, uint32_t c);
#elif defined(Z_PASCAL_VALUE_MACRO)
	#define Z_PASCAL_VALUE(p,sz,r,c) p[(r)*((r)+1)/2U + (c)]
#endif

#endif

#endif
