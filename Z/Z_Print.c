#include "Z_Print.h"

#include "Z_Pascal.h"

#include <stdio.h>
#include <string.h>

static char S[32768];

void Z_PrintReset(void)
{
	char *s = S;
	for (int i = 0; i < sizeof(S); i++)
		*(s++) = '\0';
}

void Z_PrintPascal(int32_t *p, uint32_t sz)
{
	char *s = strcat(S, "Pascal matrix:\n");
	
    s += strlen(s);
	
	#if defined(Z_PASCAL_MATRIX)
	
    for (int r = sz; r > 0; r--)
    {
        for (int c = sz; c > 0; c--)
        {
            s += sprintf(s, " %5d", *(p++));
        }
        strcat(s++, "\n");
    }
	
	#elif defined(Z_PASCAL_ARRAY)
	
    for (int r = 0; r < sz; r++)
    {
        for (int c = 0; c <= r; c++)
        {
            s += sprintf(s, " %5d", *(p++));
        }
        strcat(s++, "\n");
    }
	
	#endif
}

void Z_PrintLn(float *v, uint32_t sz, char *c)
{
	char *s = strcat(S, "");
	
    s += strlen(s);
	
    if (v)
    {
        for (int k = 0; k < sz; k++)
            if (v[k] != 0)
                s += sprintf(s, "%+8.3f%s%d", v[k], c, k);
            else
                s += sprintf(s, "%10s","");
    }
    else
    {
        for (int k = 0; k < sz; k++)
		{
            strcat(s, "----------");
			s += strlen(s);
		}
    }
	
    strcat(s++, "\n");
}

void Z_Print(char *n, float *ab, uint32_t sz, char *c)
{
    sprintf(S + strlen(S), "\n%s:\n", n);
    Z_PrintLn(&ab[ 0], sz, c);
    Z_PrintLn(  0    , sz, c);
    Z_PrintLn(&ab[sz], sz, c);
}

void Z_Printf(char *format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    vsprintf(S + strlen(S), format, argptr);
    va_end(argptr);
}
