#include "Z_Cycle.h"

#ifdef Z_LOG_ANALYZER

	volatile float __Z_LOG_FREQ;
	
	volatile float __Z_LOG_SRC;
	
	volatile float __Z_LOG_X;
	volatile float __Z_LOG_Y;
	volatile float __Z_LOG_A;
	volatile float __Z_LOG_B;
	
	volatile float __Z_LOG_EN = 0;

	#define Z_LOG_FREQ(val) if (__Z_LOG_EN) __Z_LOG_FREQ = val

	#define Z_LOG_SRC(val)  if (__Z_LOG_EN) __Z_LOG_SRC = val
	
	#define Z_LOG_X(val)    if (__Z_LOG_EN) __Z_LOG_X = val
	#define Z_LOG_Y(val)    if (__Z_LOG_EN) __Z_LOG_Y = val
	#define Z_LOG_A(val)    if (__Z_LOG_EN) __Z_LOG_A = val
	#define Z_LOG_B(val)    if (__Z_LOG_EN) __Z_LOG_B = val
	#define Z_LOG_EN(en)	__Z_LOG_EN = en

#else

	#define Z_LOG_FREQ(val)
	
	#define Z_LOG_X(val)
	#define Z_LOG_Y(val)
	#define Z_LOG_A(val)
	#define Z_LOG_B(val)
	#define Z_LOG_EN(en)

#endif

float Z_CycleDirect(float *xfn, int sz, float x)
{
//	float   *kx   = xfn + sz*2;
//	float   *ky   = xfn + sz*3;
//	float   *xPre = xfn + sz*4;
//	float   *yPre = xfn + sz*5;
	uint32_t pos  = *(uint32_t *)(xfn + sz*6);
	
//	xPre[pos] = x;
	*(xfn + sz*4 + pos) = x;
	
	float y = 0.0f;
	
	{
		float *kx   = xfn + sz*2;
		float *xx   = xfn + sz*4 + pos;
		float *xend = xfn + sz*5;
		
		for (int i = sz; i > 0; i--)
		{
			y += *(xx++) * *(kx++);
			
			if (xx >= xend)
			{
				xx = xfn + sz*4;
			}
		}
	}
	Z_LOG_X(y);
	
	{
		float *ky   = xfn + sz*3 + 1;
		float *yy   = xfn + sz*5 + pos;
		float *yend = xfn + sz*6;
		
		for (int i = sz; i > 1; i--)
		{
			if (++yy >= yend)
			{
				yy = xfn + sz*5;
			}
			
			y -= *(yy) * *(ky++);
		}
	}
	Z_LOG_Y(y);
	
//	yPre[pos] = y;
	*(xfn + sz*5 + pos) = y;
	
	if (pos > 0U)
		pos--;
	else
		pos = sz-1;
	
	*(uint32_t *)(xfn + sz*6) = pos;
	
	return y;
}

float Z_CycleCanon(float *xfn, int sz, float x)
{
//	float   *kx   = xfn + sz*2;
//	float   *ky   = xfn + sz*3;
//	float   *buf  = xfn + sz*4;
	uint32_t pos  = *(uint32_t *)(xfn + sz*6);
	
	float y = x;
	
	{
		float *ky   = xfn + sz*3 + 1;
		float *yy   = xfn + sz*4 + pos;
		float *yend = xfn + sz*5;
		
		for (int i = sz; i > 1; i--)
		{
			if (++yy >= yend)
			{
				yy = xfn + sz*4;
			}
			
			y -= *(yy) * *(ky++);
		}
	}
	Z_LOG_A(y);
	
	*(xfn + sz*4 + pos) = y;
	
	y = 0.0f;
	{
		float *kx   = xfn + sz*2;
		float *xx   = xfn + sz*4 + pos;
		float *xend = xfn + sz*5;
		
		for (int i = sz; i > 0; i--)
		{
			y += *(xx++) * *(kx++);
			
			if (xx >= xend)
			{
				xx = xfn + sz*4;
			}
		}
	}
	Z_LOG_B(y);
	
	if (pos > 0U)
		pos--;
	else
		pos = sz-1;
	
	*(uint32_t *)(xfn + sz*6) = pos;
	
	return y;
}
