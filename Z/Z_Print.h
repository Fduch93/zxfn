#ifndef __Z_PRINT
#define __Z_PRINT

#include <stdint.h>
#include <stdarg.h>

void Z_PrintReset(void);

void Z_PrintPascal(int32_t *p, uint32_t sz);

void Z_Print(char *n, float *ab, uint32_t sz, char *c);

void Z_Printf(char *format, ...);

#endif
