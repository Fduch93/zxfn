#ifndef __Z_CYCLE
#define __Z_CYCLE

#include <stdint.h>

float Z_CycleDirect(float *xfn, int sz, float x);
float Z_CycleCanon (float *xfn, int sz, float x);

#endif
