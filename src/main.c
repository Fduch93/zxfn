#include <stdint.h>
#include <stdio.h>

#include "Z_Xfn.h"
#include "Z_Cycle.h"
#include "Z_Sweep.h"
#include "Z_Step.h"
#include "Z_Print.h"

#include "SLAU.h"
#include "MATRIX.h"

//////////////////////////////////////////////////////////////////////////
#define N 256

typedef struct
{
	uint32_t Pos;
	int32_t SmplFreq;
	int32_t WaveFreq;
	int32_t sin[N];
} SIN_Gen_S;

static SIN_Gen_S SIN_Gen = {
	.sin = {0,804,1607,2410,3211,4011,4807,5601,6392,7179,7961,8739,9511,10278,11039,11792,12539,13278,14009,14732,15446,16151,16845,17530,18204,18867,19519,20159,20787,21402,22005,22594,23170,23731,24279,24811,25329,25832,26319,26790,27245,27683,28105,28510,28898,29268,29621,29956,30273,30571,30852,31113,31356,31580,31785,31971,32137,32285,32412,32521,32609,32678,32728,32757,32767,32757,32728,32678,32609,32521,32412,32285,32137,31971,31785,31580,31356,31113,30852,30571,30273,29956,29621,29268,28898,28510,28105,27683,27245,26790,26319,25832,25329,24811,24279,23731,23170,22594,22005,21402,20787,20159,19519,18867,18204,17530,16845,16151,15446,14732,14009,13278,12539,11792,11039,10278,9511,8739,7961,7179,6392,5601,4807,4011,3211,2410,1607,804,0,-805,-1608,-2411,-3212,-4012,-4808,-5602,-6393,-7180,-7962,-8740,-9512,-10279,-11040,-11793,-12540,-13279,-14010,-14733,-15447,-16152,-16846,-17531,-18205,-18868,-19520,-20160,-20788,-21403,-22006,-22595,-23171,-23732,-24280,-24812,-25330,-25833,-26320,-26791,-27246,-27684,-28106,-28511,-28899,-29269,-29622,-29957,-30274,-30572,-30853,-31114,-31357,-31581,-31786,-31972,-32138,-32286,-32413,-32522,-32610,-32679,-32729,-32758,-32768,-32758,-32729,-32679,-32610,-32522,-32413,-32286,-32138,-31972,-31786,-31581,-31357,-31114,-30853,-30572,-30274,-29957,-29622,-29269,-28899,-28511,-28106,-27684,-27246,-26791,-26320,-25833,-25330,-24812,-24280,-23732,-23171,-22595,-22006,-21403,-20788,-20160,-19520,-18868,-18205,-17531,-16846,-16152,-15447,-14733,-14010,-13279,-12540,-11793,-11040,-10279,-9512,-8740,-7962,-7180,-6393,-5602,-4808,-4012,-3212,-2411,-1608,-805}
};

int16_t SIN_Next()
{
	SIN_Gen_S *gen = &SIN_Gen;
	
	int intr = (gen->Pos >> 16) & 0xFFFFU;
	int frac = (gen->Pos >>  0) & 0xFFFFU;
	
	int s = (gen->sin[ intr            ] * (65536 - frac)
	      +  gen->sin[(intr + 1) & 0xFF] *          frac
	      + 32768)
	      / 65536;
	
	uint32_t p = gen->Pos + (gen->WaveFreq * N * 65536) / gen->SmplFreq;
	
	if (p >= N * 65536)
	    p -= N * 65536;
	
	gen->Pos = p;
	
	return s;
}

volatile int S;
void qwe()
{
	SIN_Gen.SmplFreq = 1000;
	SIN_Gen.WaveFreq = 5;
	
	while (1)
	{
		S = SIN_Next();
	}
}
//////////////////////////////////////////////////////////////////////////

int main(void)
{
	SLAU_SolveTest();
	MATRIX_DetTest();
	
	Z_PrintReset();
	Z_XfnConvertTest();
	Z_PrintReset();
//	Z_SweepTest();
//	Z_PrintReset();
//	Z_StepTest();
//	Z_PrintReset();
	Z_TestXfnRestoreZ();
	Z_PrintReset();
	
	while (1) __breakpoint(0);
}
