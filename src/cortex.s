	AREA STACK, NOINIT, READWRITE, ALIGN=3
	SPACE 0x00001000
__initial_sp

	AREA HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
__heap_limit

	EXPORT __initial_sp
	EXPORT __heap_base
	EXPORT __heap_limit

	PRESERVE8
	THUMB

	AREA RESET, DATA, READONLY

	EXPORT __Vectors
__Vectors
	DCD __initial_sp      ; Top of Stack
	DCD Reset_Handler     ; Reset Handler
	DCD NMI_Handler       ; NMI Handler
	DCD HardFault_Handler ; Hard Fault Handler
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD SVC_Handler       ; SVCall Handler
	DCD 0                 ; Reserved
	DCD 0                 ; Reserved
	DCD PendSV_Handler    ; PendSV Handler
	DCD SysTick_Handler   ; SysTick Handler

	DCD 0 ;IRQ0
	DCD 0 ;IRQ1
	DCD 0 ;IRQ2
	DCD 0 ;IRQ3
	DCD 0 ;IRQ4
	DCD 0 ;IRQ5
	DCD 0 ;IRQ6
	DCD 0 ;IRQ7
	DCD 0 ;IRQ8
	DCD 0 ;IRQ9
	DCD 0 ;IRQ10
	DCD 0 ;IRQ11
	DCD 0 ;IRQ12
	DCD 0 ;IRQ13
	DCD 0 ;IRQ14
	DCD 0 ;IRQ15
	DCD 0 ;IRQ16
	DCD 0 ;IRQ17
	DCD 0 ;IRQ18
	DCD 0 ;IRQ19
	DCD 0 ;IRQ20
	DCD 0 ;IRQ21
	DCD 0 ;IRQ22
	DCD 0 ;IRQ23
	DCD 0 ;IRQ24
	DCD 0 ;IRQ25
	DCD 0 ;IRQ26
	DCD 0 ;IRQ27
	DCD 0 ;IRQ28
	DCD 0 ;IRQ29
	DCD 0 ;IRQ30
	DCD 0 ;IRQ31

	AREA |.text|, CODE, READONLY

Reset_Handler PROC
	EXPORT Reset_Handler
	IMPORT __main
		LDR R0,=__main
		BX  R0
	ENDP

	EXPORT NMI_Handler        [WEAK]
	EXPORT HardFault_Handler  [WEAK]
	EXPORT SVC_Handler        [WEAK]
	EXPORT PendSV_Handler     [WEAK]
	EXPORT SysTick_Handler    [WEAK]

NMI_Handler PROC
	B .
	ENDP
HardFault_Handler PROC
	B .
	ENDP
SVC_Handler PROC
	B .
	ENDP
PendSV_Handler PROC
	B .
	ENDP
SysTick_Handler PROC
	B .
	ENDP

	ALIGN

	END
