#include "SLAU.h"

#include "MATRIX.h"

int SLAU_Solve(SLAU_Equasion_S *eq)
{
	if (!MATRIX_Invert(eq->Inv, eq->A, eq->n))
		return 0;
	
	MATRIX_MxV(eq->Result, eq->Inv, eq->B, eq->n);
	
	return 1;
}

#ifdef TEST

void SLAU_SolveTest(void)
{
	#define EQ_TEST_N 4
	
	float A     [MATRIX_SIZE_WITH_DET_CALC_SPACE(EQ_TEST_N)] = {0,1,3,5, 7,11,13,17, 19,23,29,31, 33,44,55,66};
	float B     [EQ_TEST_N                                 ] = {0,1,2,3};
	float Inv   [EQ_TEST_N * EQ_TEST_N                     ] = {0};
	float Result[EQ_TEST_N                                 ] = {0};
	
	SLAU_Equasion_S EQ_TEST = {
		.n = EQ_TEST_N,
		.A = A,
		.B = B,
		.Inv = Inv,
		.Result = Result
	};
	
	// wolfram inv   {{0,1,3,5},{7,11,13,17},{19,23,29,31},{33,44,55,66}}
	// wolfram solve {{0,1,3,5},{7,11,13,17},{19,23,29,31},{33,44,55,66}}*{{x1},{x2},{x3},{x4}}={0,1,2,3}
	volatile int ok = SLAU_Solve(&EQ_TEST);
	ok = ok;
}

#endif
