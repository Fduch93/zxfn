#ifndef __SLAU
#define __SLAU

#include <stdint.h>

typedef struct
{
	int    n     ;
	float *A     ; // n*n + MATRIX_SIZE_FOR_DET_CALC(n)
	float *B     ; // 1*n
	float *Inv   ; // n*n
	float *Result; // 1*n
} SLAU_Equasion_S;

int SLAU_Solve(SLAU_Equasion_S *eq);

#ifdef TEST
void SLAU_SolveTest(void);
#endif

#endif
