#include "MATRIX.h"

float MATRIX_Det(float *ff, int n)
{
	if (n == 2)
	{
		return ff[0] * ff[3]
		     - ff[1] * ff[2];
	}
	else
	{
		float d = 0;
		
		// Laplace expansion along first col
		for (int r = 0; r < n; r++)
		{
			float minor = ff[r*n] * MATRIX_Minor(ff, n, r, 0);
			
			if (r & 1)
			{
				minor = -minor;
			}
			
			d += minor;
		}
		
		return d;
	}
}

float MATRIX_Minor(float *ff, int n, int row, int col)
{
	if (n == 2)
	{
		// never supposed to be used
		return ff[3 - row*2 - col];
	}
	else
	{
		//float sub[(n-1)*(n-1)]; // result in heap allocation
		float *sub = ff + n*n; // requires ff array to be of sufficient length
		
		for (int ir = 0; ir < n; ir++)
		{
			if (ir != row)
			{
				for (int ic = 0; ic < n; ic++)
				{
					if (ic != col)
					{
						*(sub++) = *ff;
					}
					ff++;
				}
			}
			else
			{
				ff += n;
			}
		}
		
		n--;
		
		sub -= n*n;
		
		return MATRIX_Det(sub, n);
	}
}

int MATRIX_Invert(float *inv, float *ff, int n)
{
	float det = MATRIX_Det(ff, n);
	
	if (det == 0)
		return 0;
	
	for (int r = 0; r < n; r++)
	for (int c = 0; c < n; c++)
	{
		float minor = MATRIX_Minor(ff, n, r, c);
		
		if ((r+c) & 1)
			minor = -minor;
		
		inv[c*n + r] =  minor / det;
	}
	
	// for (int i = n*n - 1; i >= 0 ; i--) check for nan
	
	return 1;
}

void MATRIX_MxV(float *result, float *ff, float *v, int n)
{
	for (int i = 0; i < n; i++)
	{
		float x = 0;
		
		for (int j = 0; j < n; j++)
		{
			x += *(ff++) * v[j];
		}
		
		*(result++) = x;
	}
}

/*
void MATRIX_MxVx(float *result, float *ff, float *v, int nM, int nV)
{
	for (int i = 0; i < nM; i++)
	{
		float x = 0;
		
		for (int j = 0; j < nV; j++)
		{
			x += *(ff++) * v[j];
		}
		
		*(result++) = x;
	}
}
*/

#ifdef TEST

void MATRIX_DetTest(void)
{
	{
		// wolfram det {{2,3}, {4,5}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(2)] = {2,3, 4,5};
		volatile float det = MATRIX_Det(ff,2);
		det = det; // expected -2
	}
	{
		// wolfram det {{0,1,2,3}, {4,5,6,7}, {9,10,11,12}, {13,14,15,16}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(4)] = {0,1,2,3, 4,5,6,7, 9,10,11,12, 13,14,15,16};
		volatile float det = MATRIX_Det(ff,4);
		det = det; // expected 0
	}
	{
		// wolfram det {{0,1,3,5}, {7,11,13,17}, {19,23,29,31}, {33,44,55,66}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(4)] = {0,1,3,5, 7,11,13,17, 19,23,29,31, 33,44,55,66};
		volatile float det = MATRIX_Det(ff,4);
		det = det; // expected -198
	}
	{
		// wolfram det {{1,2,3}, {3,2,1}, {1,3,1}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(3)] = {1,2,3, 3,2,1, 1,3,1};
		volatile float det = MATRIX_Det(ff,3);
		det = det; // expected 16
	}
	{
		// wolfram inv {{0,1,3,5}, {7,11,13,17}, {19,23,29,31}, {33,44,55,66}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(4)] = {0,1,3,5, 7,11,13,17, 19,23,29,31, 33,44,55,66};
		float inv[4*4];
		volatile int ok = MATRIX_Invert(inv, ff, 4);
		ok = ok; // expected ok 1, expected inv:
		//   -0.55555  -1.55555  -0.77777   0.808080
		//   -0.22222   1.27777   0.38888  -0.494949
		//    0.77777   0.77777   0.88888  -0.676767
		//   -0.22222  -0.72222   0.61111   0.505050
	}
	{
		// wolfram inv {{2,3}, {4,5}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(2)] = {2,3, 4,5};
		float inv[2*2];
		volatile int ok = MATRIX_Invert(inv, ff, 2);
		ok = ok; // expected ok 1, expected inv:
		//   -2.5   1.5
		//    2.0  -1.0
	}
	{
		// wolfram det {{1,2,3}, {3,2,1}, {1,3,1}}
		float ff[MATRIX_SIZE_WITH_DET_CALC_SPACE(3)] = {1,2,3, 3,2,1, 1,3,1};
		float inv[3*3];
		volatile int ok = MATRIX_Invert(inv, ff, 3);
		ok = ok; // expected ok 1, expected inv:
		// -0.0625   0.4375  -0.25
		// -0.125   -0.125    0.5
		//  0.4375  -0.0625  -0.25
	}
}

#endif
