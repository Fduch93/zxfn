#ifndef __MATRIX
#define __MATRIX

#include <stdint.h>

// wolfram sum a^2 from 2 to n = (2*n^3 + 3*n^2 + n - 6) / 6
#define MATRIX_SUM_N_SQUARED(n) ((2*(n)*(n)*(n) + 3*(n)*(n) + n - 6 + 3) / 6)

#define MATRIX_SIZE_WITH_DET_CALC_SPACE(n) MATRIX_SUM_N_SQUARED(n)

#define MATRIX_SIZE_FOR_DET_CALC(n) MATRIX_SUM_N_SQUARED(n-1)

#define MATRIX_SIZE_FOR_INV(n) (n*n)

float MATRIX_Det   (float *ff, int n);
float MATRIX_Minor (float *ff, int n, int row, int col);
int   MATRIX_Invert(float *inv, float *ff, int n);
void  MATRIX_MxV   (float *result, float *ff, float *v, int n);

#ifdef TEST
void MATRIX_DetTest(void);
#endif

#endif
