Const ForReading = 1   
Const ForWriting = 2

Set oShell = CreateObject("WScript.Shell")
Set fso    = CreateObject("Scripting.FileSystemObject")

If (fso.FileExists("qwe.bin")) Then
	fso.DeleteFile "qwe.bin", True
End If
If (fso.FileExists("qwe.csv")) Then
	fso.DeleteFile "qwe.csv", True
End If

oShell.Run "hex2bin.exe qwe.hex", 0, True  'hide window, wait for return
fso.MoveFile "qwe.bin", "qwe.csv"

Set oFile = fso.OpenTextFile("qwe.csv", 1) 'ForReading
strText = oFile.ReadAll
oFile.Close

' strNewText = Replace(strText, ".", ",")
strNewText = Replace(strText, ";", ",")

Set oFile = fso.OpenTextFile("qwe.csv", 2) 'ForWriting
oFile.Write strNewText  'WriteLine adds extra CR/LF
oFile.Close

'oShell.Run "qwe.csv"

Dim g, oWorkbook, oExcel

const xlDelimited      = 1
const xlTextQualifierNone = -4142
const xlDelimitedSemicolons = 4

Set oExcel = CreateObject("Excel.Application")
oExcel.Visible = True
oExcel.Workbooks.OpenText fso.GetAbsolutePathName(".") & "\" & "qwe.csv", , , xlDelimited, xlTextQualifierNone, False, False, True, False, False, False 
Set oWorkbook = oExcel.ActiveWorkbook
Set oSheet    = oWorkbook.ActiveSheet

const xlXYScatter                = -4169 'Scatter.
const xlXYScatterLines           =    74 'Scatter with Lines.
const xlXYScatterLinesNoMarkers  =    75 'Scatter with Lines and No Data Markers.
const xlXYScatterSmooth          =    72 'Scatter with Smoothed Lines.
const xlXYScatterSmoothNoMarkers =    73 'Scatter with Smoothed Lines and No Data Markers.

Set g = oSheet.ChartObjects.Add(200, 0, 720, 480).Chart

g.ChartType = xlXYScatterLinesNoMarkers

xlUp = -4162

lastRow = oSheet.Range("A" & oSheet.Rows.Count).End(xlUp).Row

Set s = g.SeriesCollection.NewSeries
s.Name    = "Прямое"
s.XValues = oSheet.Range("A1:A" & lastRow)
s.Values  = oSheet.Range("B1:B" & lastRow)

Set s = g.SeriesCollection.NewSeries
s.Name    = "Каноническое"
s.XValues = oSheet.Range("A1:A" & lastRow)
s.Values  = oSheet.Range("C1:C" & lastRow)

xlCategory 		= 1 	'Axis displays categories.
xlValue 		= 2 	'Axis displays values.
xlSeriesAxis 	= 3 	'Axis displays data series.

xlScaleLinear 		= -4132
xlScaleLogarithmic 	= -4133

oExcel.ActiveWindow.Activate

g.Axes(xlCategory).ScaleType    = xlScaleLogarithmic
g.Axes(xlCategory).MinimumScale = oSheet.Cells(1,1).Value
g.Axes(xlValue).ScaleType       = xlScaleLogarithmic

' g.ChartType = xlXYScatterLinesNoMarkers
' g.SetSourceData oSheet.Range("A:C")

' oWorkbook.ActiveChart.Legend.Select
' oExcel.Selection.Delete

' oWorkbook.ActiveChart.ChartArea.Select


' oWorkbook.ActiveChart.Axes(xlCategory).Select
' oExcel.Selection.Delete
' oWorkbook.ActiveChart.ChartArea.Select
' oWorkbook.ActiveChart.Axes(xlValue).Select
' oWorkbook.ActiveChart.Axes(xlValue).MaximumScale = 300
' oWorkbook.ActiveChart.Axes(xlValue).MinimumScale = 0
