#include <stdio.h>

#define SZ 6
#define P_SZ 8

int P[P_SZ*P_SZ] = {0};

void initP()
{
    for (int r = 0; r < P_SZ; r++)
    {
        P[r*P_SZ+0] = 1;
        for (int c = 1; c <= r; c++)
        {
            P[r*P_SZ+c] = 
                P[(r-1)*P_SZ+c] +
                P[(r-1)*P_SZ+c-1];
        }
    }
}
void printP()
{
    printf("\nPascal matrix:");
    for (int r = 0; r < P_SZ; r++)
    {
        printf("\n");
        for (int c = 0; c < P_SZ; c++)
        {
            printf(" %5d", P[r*P_SZ+c]);
        }
    }
    printf("\n");
}
void normalize(float *a, float *b)
{
    a[0] /= b[0];
    for (int k = 1; k < SZ; k++)
    {
        a[k]/=b[0];
        b[k]/=b[0];
    }
    b[0]=1;
}
void printxln(float *v, char *c)
{
    if (v)
    {
        for (int k = 0; k < SZ; k++)
            if (v[k]!=0)
                printf("%+8.3f%s%d",v[k],c,k);
            else
                printf("%10s","");
    }
    else
    {
        for (int k = 0; k < SZ; k++)
            printf("----------");
    }
    printf("\n");
}
void printx(char *n, float *a, float *b, char *c)
{
    printf("\n%s:\n", n);
    printxln(a,c);
    printxln(0,c);
    printxln(b,c);
}
void s2z(float *a, float *b, float *A, float *B, float c)
{
    for (int k = 0; k < SZ; k++)
        A[k]=B[k]=0;
    
    for (int k = 0; k < SZ; k++)
    {
        for (int i = 0; i < SZ; i++)
        {
            float x = 0;
            for (int p = 0; p <= i; p++)
            for (int q = 0; q <= SZ-1-i; q++)
                if (p+q == k)
                    x += P[(     i)*P_SZ+p]
                       * P[(SZ-1-i)*P_SZ+q]
                       * ((p%2)*2-1);
            for (int e = 0; e < i; e++)
                x *= c;
            A[k] += a[i] * x;
            B[k] += b[i] * x;
        }
    }
    
    normalize(A,B);
    
    printx("Source s-xfn",a,b,"s");
    printx("Converted z-xfn",A,B,"ż");
}
void z2s(float *A, float *B, float *a, float *b, float c)
{
    for (int k = 0; k < SZ; k++)
        a[k]=b[k]=0;
    
    for (int k = 0; k < SZ; k++)
    {
        for (int i = 0; i < SZ; i++)
        {
            float x = 0;
            for (int p = 0; p <= i; p++)
            for (int q = 0; q <= SZ-1-i; q++)
            {
                if (p+q == k)
                {
                    float xx = P[(     i)*P_SZ+p]
                             * P[(SZ-1-i)*P_SZ+q]
                             * ((p%2)*2-1);
                    for (int e = 0; e < SZ-1-p-q; e++)
                        xx *= c;
                    x += xx;
                }
            }
            a[k] += A[i] * x;
            b[k] += B[i] * x;
        }
    }
    
    normalize(a,b);
    
    printx("Source z-xfn",A,B,"ż");
    printx("Converted s-xfn",a,b,"s");
}
int main() {
    printf("Hello, World!\n");
    
    initP();
    printP();
    
    float a[SZ] = {0.204,0,0.167,0,0.0297,0};
    float b[SZ] = {0.204,0.747,1.097,1.815,0.925,1};
    float A[SZ];
    float B[SZ];
    float X[SZ];
    float Y[SZ];
    
    float C = 3.077683537175253f;
    
    s2z(a,b,A,B,C);
    
    normalize(a,b);
    
    printx("Normalized s-xfn",a,b,"s");
    
    z2s(A,B,a,b,C);
    
    return 0;
}